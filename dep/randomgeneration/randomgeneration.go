package randomgeneration

import "math/rand"

func GenRandomSlice(len int) []int {
	result := make([]int, len)
	for n := range result {
		result[n] = rand.Intn(1000)
	}
	return result
}