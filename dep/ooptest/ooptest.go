package ooptest

import (
	"fmt"
)

type Box struct {
	Length int
	Width int
	Depth int
}

func (x Box) GetVolume() int {
	return x.Length*x.Width*x.Depth
}

//Lowercase so it is not exported, like a private function
func (x Box) printIt() {
	fmt.Printf("Box is %vx%vx%v\n", x.Length, x.Width, x.Depth)
}

func (x Box) ToString() {
	x.printIt()
}