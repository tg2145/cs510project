package basicmath

import "errors"

// uppercase name means exportable
func Add(a int, b int) int { 
	return a + b
}

func Subtract(a int, b int) int {
	return a - b
}

func Multiply(a int, b int) int {
	return a * b
}

// this is how to do error handling
func Divide(a int, b int) (int, error) {
	if b == 0 {
		return 0, errors.New("can't divide by zero")
	}
	return a / b, nil
}

func AddSlices(a []int, b []int) ([]int, error) {
	results := make([]int,len(a))
	if len(a) != len(b) {
		return results, errors.New("slice sizes do not match")
	}
	for n := 0; n < len(a); n++ {
		results[n] = a[n] + b[n]
	}
	return results, nil
}

func AddPartialSlices(a []int, b []int, start int, end int) ([]int, error) {
	results := make([]int,end-start)
	if end < start {
		return results, errors.New("bad index")
	}
	for n := 0; n < end-start; n++ {
		results[n] = a[n+start] + b[n+start]
	}
	return results, nil
}