package main

// #include <stdio.h>
// typedef int (*intFunc) ();
//
// int
// bridge_int_func(intFunc f)
// {
//		return f();
// }
//
// int fortytwo()
// {
//	    return 42;
// }
//void printmessage()
//{
//	printf("Hello from C code\n");
//}
import "C"
//the import C must be on its own right after the C code comments
import (
	"fmt"
	"sync"
	"time"
	"reflect"
	"bitbucket.org/tg2145/cs510project.git/dep/basicmath"
	"bitbucket.org/tg2145/cs510project.git/dep/randomgeneration"
	"bitbucket.org/tg2145/cs510project.git/dep/ooptest"
	"github.com/guptarohit/asciigraph"
)

func Example1() {
	// := lets you declare and define to avoid "var result int"
	result := basicmath.Add(10, 5) 
	fmt.Printf("Add 10 5 is %v\n", result)
	result = basicmath.Subtract(10, 5)
	fmt.Printf("Subtract 10 5 is %v\n", result)
	result = basicmath.Multiply(10, 5)
	fmt.Printf("Multiply 10 5 is %v\n", result)
	// interestingly := still works as long as one variable is new
	result, err := basicmath.Divide(10, 5)
	fmt.Printf("Divide 10 5 is %v\n", result)
	result, err = basicmath.Divide(10, 0)
	if err != nil {
		// I don't want to exit the program
		// But this is how you would do it
		//log.Fatal(err)
		fmt.Println(err)
	} else { //yes this has to be formatted like this
		fmt.Printf("Divide 10 0 is %v\n", result)
	}
}

func Example2() {
	// these are slices, not arrays, since I did not specify a size
	// arrays should be avoided in Go since it is not a ref type
	inputA := []int{1,2,3}
	inputB := []int{4,5,6}
	results, err := basicmath.AddSlices(inputA, inputB)
	if err != nil {
		//log.Fatal(err)
		fmt.Println(err)
	} else {
		// easy way to print a slice
		fmt.Printf("%v\n", results)
	}
	//purposely causing error
	inputB = append(inputB, 8)
	results, err = basicmath.AddSlices(inputA, inputB)
	if err != nil {
		//log.Fatal(err)
		fmt.Println(err)
	} else {
		fmt.Printf("%v\n", results)
	}

	largeSliceA := randomgeneration.GenRandomSlice(100000)
	largeSliceB := randomgeneration.GenRandomSlice(100000)

	//how to measure time
	start := time.Now()
	largeSliceC,err := basicmath.AddSlices(largeSliceA,largeSliceB)
	end := time.Now()

	fmt.Printf("Serial took: %v\n", end.Sub(start))

	start = time.Now()
	largeSliceD := make([]int,len(largeSliceA))
	//how to make threads
	threads := 4
	var wg sync.WaitGroup
	wg.Add(threads) // how many done calls to wait for

	for n := 0; n < threads; n++ {
		go func(n int) {
			startIndex := len(largeSliceA)/threads*n
			endIndex := len(largeSliceA)/threads*(n+1)
			largeSliceTemp,err := basicmath.AddPartialSlices(largeSliceA,largeSliceB,startIndex,endIndex)
			if err != nil {
				fmt.Println(err)
			}
			//easy to work with index ranges unlike C++
			copy(largeSliceD[startIndex:],largeSliceTemp)
			defer wg.Done()
		} (n)
	}

	wg.Wait()
	end = time.Now()
	fmt.Printf("Parallel took: %v\n", end.Sub(start))
	fmt.Printf("Results Match: %v\n", reflect.DeepEqual(largeSliceC,largeSliceD))
	//parallel is slower because I am not efficiently allocating slices
	//also the serial code is simple enough to be optimized automatically

}

func Example3() {
	rect := Rectangle{5,4}
	fmt.Printf("Area of 5x4 rectangle is: %v\n", rect.GetArea())
	fmt.Printf("Perimiter of 5x4 rectangle is: %v\n", rect.GetPerimiter())
	rect.SetWidth(10)
	fmt.Printf("Changing width from 4 to 10\n")
	fmt.Printf("Area of 5x10 rectangle is: %v\n", rect.GetArea())
	rect.printIt()

	box := ooptest.Box{5,6,7}
	fmt.Printf("Volume of 5x6x7 box is: %v\n", box.GetVolume())
	box.ToString() //Box is 5x6x7
	//box.printIt() results in: box.printIt undefined (cannot refer to unexported field or method ooptest.Box.printIt)
}

func Example4() {

	fmt.Printf("Now trying to plot with external library\n")
	data := []float64{3, 4, 9, 6, 2, 4, 5, 8, 5, 10, 2, 7, 2, 5, 6}
    graph := asciigraph.Plot(data)

    fmt.Println(graph)
}

func Example5() {
	C.printmessage()
	f := C.intFunc(C.fortytwo)
	fmt.Println(int(C.bridge_int_func(f)))
}