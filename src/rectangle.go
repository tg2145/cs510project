package main

import (
	"fmt"
)

//example of how you can mimic basic OOP in Go
type Rectangle struct {
	length int
	width int
}

func (x Rectangle) GetArea() int {
	return x.width*x.length;
}

func (x Rectangle) GetPerimiter() int {
	return 2*x.width+2*x.length;
}

// need to use pointer to change the fields
func (x *Rectangle) SetWidth(y int) {
	x.width = y
}

func (x *Rectangle) SetLength(y int) {
	x.length = y
}

//Note that this non-exportable function can still be used
//This is because it is in the main package, so everything in main can see it
func (x Rectangle) printIt() {
	fmt.Printf("Length is %v\n", x.length)
	fmt.Printf("Width is %v\n", x.width)
}

func (x Rectangle) ToString() {
	x.printIt()
}