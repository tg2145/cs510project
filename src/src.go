package main

import (
	"log"
)

func main() { // bracket needs to be here
	log.SetPrefix("main: ")
	// original flags will insert time of log to each printout
	log.SetFlags(0)
	//shows how to call functions and print
	Example1()
	//shows how to make and operation on slices
	//shows how to make worker threads
	Example2()
	//shows how to mimic oop
	Example3()
	//how to add someone elses library
	Example4()
	//How to call C code from Go
	Example5()
}
