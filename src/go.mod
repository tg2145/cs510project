module bitbucket.org/tg2145/cs510project.git/src

go 1.13

replace bitbucket.org/tg2145/cs510project.git/dep/basicmath => ../dep/basicmath

replace bitbucket.org/tg2145/cs510project.git/dep/randomgeneration => ../dep/randomgeneration

require (
	bitbucket.org/tg2145/cs510project.git/dep/basicmath v0.0.0-00010101000000-000000000000
	bitbucket.org/tg2145/cs510project.git/dep/ooptest v0.0.0-00010101000000-000000000000
	bitbucket.org/tg2145/cs510project.git/dep/randomgeneration v0.0.0-00010101000000-000000000000
	github.com/guptarohit/asciigraph v0.5.2
)

replace bitbucket.org/tg2145/cs510project.git/dep/ooptest => ../dep/ooptest
